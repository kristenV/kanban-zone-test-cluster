# Kanbanzone Service Test Cluster

## Prerequisites

### Docker for Mac
Install the latest version of Docker for your environment. It is possible that you may need to uninstall the version you have already installed. [Docker installation for Mac can be found here.](https://docs.docker.com/docker-for-mac/install/)

### Docker Compose for Mac
Install the latest version of Docker Compose for your environment. It is possible that you may need to uninstall the version you have already installed. [Docker Compose installation for Mac can be found here.](https://docs.docker.com/compose/install/)

### AWS Credentials
The cluster is configured to retrieve the service configuration from the `kanbanzone_atf` bucket. Please make sure your AWS credentials are configured properly.

## Creating a cluster
* Create a docker image from the services version you want to test. To create a docker image you need to execute the following from the kanbanzone services folder:
```
docker build -t kanbanzone-service:1 . 
```
    Please note the period at the end is necessary and it is part of the command. The number one (1) at the end represents a version number, you can change the version number to whatever version you desire.

* Once an image has been created, open a terminal and change to the folder where the docker-compose.yml file resides.
* Execute the following commands:
```
docker swarm init
docker stack deploy --compose-file=docker-compose.yml kz_test
```
This should execute a cluster (called stack) with 2 kanbanzone-service nodes and a load balancer between the two nodes. The load balancer should be running at localhost, port 80. To test the cluster is up an running, try to reach the service health check by running the curl command:
```
curl http://localhost/services-health-check
```

## Useful commands
* To check the services running in the kz_test stack
```
docker service ls
```
* To check the logs from the kanbanzone services
```
docker service logs kz_test_kzsvc
```
* To check the logs from a specific node from the kanbanzone services (replace the number with the node you want)
```
docker service logs kz_test_kzsvc|grep kzsvc\.1
```
* To check the logs from the load balancer
```
docker service logs kz_test_lb
```
* To stop the cluster
```
docker stack rm kz_test
```
* To add more nodes to the cluster (i.e. Run 20 nodes at a time)
```
docker service scale kz_test_kzsvc=20
```
* To update the cluster with a new version without shutting down the cluster:
    * First create a new image with the new version. Run the command a the kanban-zone-services folder
```
docker build -t kanbanzone-service:2 . 
```

*   * Update the cluster with the new version:

```
docker service update --image kanbanzone-service:2 kz_test_kzsvc
```




